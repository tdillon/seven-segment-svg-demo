# Seven Segment SVG Demo

Visit https://tdillon.gitlab.io/seven-segment-svg-demo/7.svg to see SVG examples of different [seven segment](https://www.npmjs.com/package/seven-segment) configurations.

Here is a simple SVG document of what a single segment would look like.

```svg
<svg version="1.1" width="100" height="16" xmlns="http://www.w3.org/2000/svg">
  <path d="M 29.905901179086154,7.788409277835531
           L 39.18776591654073,0
           L 85.6779240492809,0
           L 92.2131754017704,7.788409277835531
           L 82.93131066431583,15.576818555671062
           L 36.44115253157565,15.576818555671062
           Z" />
</svg>
```
