import { Digit, Seven } from "https://esm.sh/seven-segment@4.1.3";

const VERTICAL_PADDING = 75

/**
 * Return the SVG path element for an individual seven-segment digit.
 */
function getSVGPath(s: Seven, x: number, y: number): string {
    return `
  <!-- Digit: ${Digit[s.digit]} Height: ${s.height} Width: ${s.width} Angle: ${s.angle} LtoW: ${s.ratioLtoW} LtoS: ${s.ratioLtoS} -->
${s.segments.map(seg => `  <path d="M ${seg.points[0].x + x},${seg.points[0].y + y}
           L ${seg.points[1].x + x},${seg.points[1].y + y}
           L ${seg.points[2].x + x},${seg.points[2].y + y}
           L ${seg.points[3].x + x},${seg.points[3].y + y}
           L ${seg.points[4].x + x},${seg.points[4].y + y}
           L ${seg.points[5].x + x},${seg.points[5].y + y}
           Z" class="${seg.on ? 'on' : 'off'}" />`).join('\n')}`
}

/**
 * Generate SVG path elements for all combinations of the following settings.
 *
 * Angle: min to max with 10 degree increments
 * Length to Width ratio: 2, 4, 6, 8, 10
 * Length to Spacing ratio: 32
 * Width: 100
 * @returns SVG paths for all generated seven segments as well as the overall height
 */
function getAllSevenSegmentDemoPaths() {
    const allPaths = []
    const x = new Seven({ width: 100 });
    let top = 0

    for (let angle = -80; angle <= 80; angle += 10) {
        for (let ltow = 2; ltow <= 10; ltow += 2) {
            try {
                x.angle = 0
                x.ratioLtoW = ltow
                x.angle = angle
                top += x.height + VERTICAL_PADDING

                allPaths.push([0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(d => {
                    x.digit = d;
                    return getSVGPath(x, 110 * d, top)
                }).join('\n'))
            } catch {
                // Some angle/ltow combinations create invalid geometry, ignore.
            }
        }
    }

    return { height: top + x.height + VERTICAL_PADDING, paths: allPaths.join('\n') }
}


const demo = getAllSevenSegmentDemoPaths()

console.log(`
<svg version="1.1" width="1100" height="${demo.height}" xmlns="http://www.w3.org/2000/svg">

  <style>
    path.on {
      fill: #111;
    }
    path.off {
       fill: #555;
    }
    rect {
        fill: #666;
    }
  </style>

  <rect width="100%" height="100%" />
  ${demo.paths}

</svg>
`)
